#include <opencv2/opencv.hpp>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace cv;
using namespace std;

bool compContorA(std::vector<cv::Point> contour1, std::vector<cv::Point> contour2) {
	double i = fabs(contourArea(cv::Mat(contour1)));
	double j = fabs(contourArea(cv::Mat(contour2)));
	return (i > j);
}

bool compXCord(Point p1, Point p2)
{
	return (p1.x < p2.x);
}

bool compYCord(Point p1, Point p2)
{
	return (p1.y < p2.y);
}

bool compDist(pair<Point, Point> p1, pair<Point, Point> p2)
{
	return (norm(p1.first - p1.second) < norm(p2.first - p2.second));
}

double dist(Point p1, Point p2)
{
	return sqrt(((p1.x - p2.x) * (p1.x - p2.x)) +
		((p1.y - p2.y) * (p1.y - p2.y)));
}

void resizeToH(Mat src, Mat& dst, int height)
{
	Size s = Size(src.cols * (height / double(src.rows)), height);
	resize(src, dst, s, INTER_AREA);
}

void orderPoints(vector<Point> inpts, vector<Point>& ordered)
{
	sort(inpts.begin(), inpts.end(), compXCord);
	vector<Point> lm(inpts.begin(), inpts.begin() + 2);
	vector<Point> rm(inpts.end() - 2, inpts.end());

	sort(lm.begin(), lm.end(), compYCord);
	Point tl(lm[0]);
	Point bl(lm[1]);
	vector<pair<Point, Point> > tmp;
	for (size_t i = 0; i < rm.size(); i++)
	{
		tmp.push_back(make_pair(tl, rm[i]));
	}

	sort(tmp.begin(), tmp.end(), compDist);
	Point tr(tmp[0].second);
	Point br(tmp[1].second);

	ordered.push_back(tl);
	ordered.push_back(tr);
	ordered.push_back(br);
	ordered.push_back(bl);
}

void Transform(Mat src, Mat& dst, vector<Point> pts)
{
	vector<Point> ordered_pts;
	orderPoints(pts, ordered_pts);

	double wa = dist(ordered_pts[2], ordered_pts[3]);
	double wb = dist(ordered_pts[1], ordered_pts[0]);
	double mw = max(wa, wb);

	double ha = dist(ordered_pts[1], ordered_pts[2]);
	double hb = dist(ordered_pts[0], ordered_pts[3]);
	double mh = max(ha, hb);

	Point2f src_[] =
	{
			Point2f(ordered_pts[0].x, ordered_pts[0].y),
			Point2f(ordered_pts[1].x, ordered_pts[1].y),
			Point2f(ordered_pts[2].x, ordered_pts[2].y),
			Point2f(ordered_pts[3].x, ordered_pts[3].y),
	};
	Point2f dst_[] =
	{
			Point2f(0,0),
			Point2f(mw - 1, 0),
			Point2f(mw - 1, mh - 1),
			Point2f(0, mh - 1)
	};
	Mat m = getPerspectiveTransform(src_, dst_);
	warpPerspective(src, dst, m, Size(mw, mh));
}

void preprocessing(Mat src, Mat& dst)
{
	cv::Mat imageGrayed;
	cv::Mat imageOpen, imageClosed, imageBlurred;

	cvtColor(src, imageGrayed, COLOR_BGR2GRAY);
	
	cv::Mat structuringElmt = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(4, 4));
	morphologyEx(imageGrayed, imageOpen, cv::MORPH_OPEN, structuringElmt);
	
	morphologyEx(imageOpen, imageClosed, cv::MORPH_CLOSE, structuringElmt);
	imshow("CourseWork", imageClosed);
	waitKey(0);
	GaussianBlur(imageClosed, imageBlurred, Size(7, 7), 0);
	/*imshow("CourseWork", imageBlurred);
	waitKey(0);*/
	Canny(imageBlurred, dst, 75, 100);
	
}

string getOutputFileN(string path, string name)
{
	std::string fname, ext;

	size_t sep = path.find_last_of("\\/");
	if (sep != std::string::npos)
	{
		path = path.substr(sep + 1, path.size() - sep - 1);

		size_t dot = path.find_last_of(".");
		if (dot != std::string::npos)
		{
			fname = path.substr(0, dot);
			ext = path.substr(dot, path.size() - dot);
		}
		else
		{
			fname = path;
			ext = "";
		}
	}

	return fname + "_" + name + ext;
}

int main(int argc, char** argv)
{
	setlocale(LC_ALL, "Russian");
	string inputName;
	std::cout << "������� ��� ����������� ��� ����������: ";
	std::cin >> inputName;
	string path = "Images/" + inputName + ".jpg";
	Mat image = imread(path);
	/*imshow("CourseWork", image);
	waitKey(0);*/
	if (image.empty())
	{
		printf("�� ���� ��������� �����������: %s\n", path.c_str());
		return -1;
	}

	double ratio = image.rows / 500;
	Mat orig = image.clone();
	resizeToH(image, image, 600);

	Mat gray, edged, warped;
	preprocessing(image, edged);

	imwrite(getOutputFileN(path, "Canny"), edged);


	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	vector<vector<Point> > approx;
	findContours(edged, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE);
	
	approx.resize(contours.size());
	size_t i, j;
	size_t height, width;
	for (i = 0; i < contours.size(); i++)
	{
		double peri = arcLength(contours[i], true);
		approxPolyDP(contours[i], approx[i], 0.02 * peri, true);
	}
	sort(approx.begin(), approx.end(), compContorA);

	for (i = 0; i < approx.size(); i++)
	{
		drawContours(image, approx, i, Scalar(255, 255, 0), 2);
		if (approx[i].size() == 4)
		{
			break;
		}
	}

	
	
	

	if (i < approx.size())
	{
		drawContours(image, approx, i, Scalar(0, 255, 0), 2);
		Rect a;
		a = cv::boundingRect(approx[i]);
		imwrite(getOutputFileN(path, "contour"), image);

		for (j = 0; j < approx[i].size(); j++)
		{
			approx[i][j] *= ratio;
		}
		
		Transform(orig, warped, approx[i]);
		
		
		
		//a.angle = 0;
		
		//fillPoly(orig, approx[i], Scalar(0, 255, 0));
		
		imshow("CourseWork", orig);
		waitKey(0);

		
		

		imwrite(getOutputFileN(path, "resized"), warped);
		
		cvtColor(warped, warped, COLOR_BGR2GRAY, 1);
		cvtColor(orig, orig, COLOR_BGR2GRAY, 1);
		adaptiveThreshold(warped, warped, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 9, 15);
		adaptiveThreshold(orig, orig, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 9, 15);
		GaussianBlur(warped, warped, Size(3, 3), 0);
		GaussianBlur(orig, orig, Size(3, 3), 0);
		imwrite(getOutputFileN(path, "scanned1"), warped);
		imwrite(getOutputFileN(path, "scanned2"), orig);

		
		

		
		
		/*RotatedRect a;
		a.center = Point2f(centerX);
		a.size.width = boundingRect.width;
		a.size.height = boundingRect.height;*/
		
		/*int res = rotatedRectangleIntersection(a, b, inter);
		if (inter.empty() || res == cv::INTERSECT_NONE)
			return 0.0f;
		if (res == cv::INTERSECT_FULL)
			return 1.0f;*/

		//float IOU = (float)(inter.area()) / (float)(a.area() + b.area() - inter.area());
		
		//Rect a = Rect(Point2i(42, 15), Point2i(421, 583));
		Rect b = Rect(Point2i(48, 66), Point2i(407, 558)); // ���� �������� ��������� �������
		Rect inter;
		inter = a & b;
		float areaInter = inter.area();
		float areaB = b.area();
		float areaA = a.area();
		float IOU = areaInter / (areaA + areaB - areaInter);
		//cout << areaInter << endl;
		//cout << areaA << endl;
		//cout << areaB << endl;
		cout << "IOU = " << IOU << endl;
	}
	
	
}
